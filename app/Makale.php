<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Makale extends Model
{
    protected $fillable=[
        'adi','dergi_id','yazar_id','hakem_id','alan','aciklama','url','hakem_onay','editor_onay',"mesaj",'updated_at','created_at'
    ];
    protected $table='makale';
}
