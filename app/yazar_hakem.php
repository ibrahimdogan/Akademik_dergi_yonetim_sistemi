<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class yazar_hakem extends Model
{
    protected $fillable=[
        'adi','soyadi','email','sifre','alan','biyografi','updated_at','created_at'
    ];
    protected $table='yazar';
}
