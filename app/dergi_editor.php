<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dergi_editor extends Model
{
    protected $fillable=[
        'adi','editor_adi','editor_email','editor_sifre','aciklama','updated_at','created_at'
    ];
    protected $table='dergi';
}
