<?php

namespace App\Http\Controllers;
use App\Makale;

use App\yazar_hakem;
use App\dergi_editor;

use Illuminate\Http\Request;
use DB;
class yazar_hakem_Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   

    public function index()
    {
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
        $makale_listele=DB::table("makale")->where('hakem_id',session()->get('yazar_id'))->get();
        session()->put('yazar_hakem_secim','hakem');
        return view('yazar.Hakem_Takip',['makaleler'=>$makale_listele,'hakem'=>1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
        return view('yazar.YeniYazarEkle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
 
        session_start();
        $varmi=DB::table("yazar")->where("email",$request->email)->get();
         if(strlen($varmi)!=2){
             
            session()->put("hata","Zaten Kayıtlısınız");
                 return redirect("bulunamadı");
         }
        

         $Veri_Ekle=new yazar_hakem();
         $Veri_Ekle->adi=$request->adi;
         $Veri_Ekle->soyadi=$request->soyadi;
         $Veri_Ekle->email=$request->email;
         $Veri_Ekle->alan=$request->alan;
         $Veri_Ekle->sifre=$request->sifre;
         $Veri_Ekle->biyografi=$request->biyografi;
         $Veri_Ekle->save();
         return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\yazar_hakem  $yazar_hakem
     * @return \Illuminate\Http\Response
     */
    public function show(yazar_hakem $yazar_hakem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\yazar_hakem  $yazar_hakem
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\yazar_hakem  $yazar_hakem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
        $guncelle=Makale::find($id);
        if($request->onaylama_durumu==1){
         $guncelle->hakem_onay=1;
         $guncelle->mesaj="Makale Başarılı";
         $guncelle->save();
        }
        elseif($request->onaylama_durumu==3){
         $guncelle->mesaj=$request->mesaj;
         $guncelle->hakem_id=0;
         $guncelle->hakem_onay=0;

         $guncelle->save();
        }
        else{
         $guncelle->hakem_onay=0;
         $guncelle->mesaj=$request->mesaj;
         $guncelle->save();
        }
         return redirect("yazar");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\yazar_hakem  $yazar_hakem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function giris_kontrol(Request $request)
    {
        $dergi_id=DB::table('dergi')->where('editor_email',$request->email)->pluck('id');
        $dergi_adi=DB::table('dergi')->where('editor_email',$request->email)->pluck('editor_adi');
        $yazar_adi=DB::table('yazar')->where('email',$request->email)->pluck('adi');
         $yazar_id=DB::table('yazar')->where('email',$request->email)->pluck('id');
         if(strlen($yazar_adi)!=2){
           session()->put('tur',"yazar");
            session()->put('adi',$yazar_adi[0]);
            session()->put('email',$request->email);
            session()->put('yazar_id',$yazar_id[0]);
            return redirect("/");
        }
         else{
            if(strlen($dergi_adi)!=2){
                 echo strlen($yazar_adi);
                session()->put('tur',"editor");
                 session()->put('adi',$dergi_adi[0]);
                 session()->put('dergi_id',$dergi_id[0]);
                 session()->put('email',$request->email);
                 return redirect("/");
             }
             else{
                session()->put("hata","Kullanıcı adı veya şifre yanlış");
                 return redirect("bulunamadı");
             }
         }
        }
    public function cikis()
    {
        session()->flush();
        return redirect('/');
    }

    public function yazar_onayla($id){
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
        $makale_onayla=DB::table('makale')->where('id',$id)->get();
        return view('yazar.Hakem_onay',['makale_onay'=>$makale_onayla]);
    }

    public function yazar_profil(){
      $yazar_bilgi=yazar_hakem::find(session()->get('yazar_id'));
      $makaleleri=DB::table("makale")->where('yazar_id',session()->get('yazar_id'))->get();
      return view("yazar.Yazar_profil",['yazar_bilgi'=>$yazar_bilgi,'makaleler'=>$makaleleri]);
      
    }
}
