<?php

namespace App\Http\Controllers;
use App\dergi_editor;

use App\makale_paylas;
use Illuminate\Http\Request;
use DB;
class makale_paylas_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $bilgiler= session()->get('dergi_bilgisi');
       $dergide_bulunan_makaleler=DB::table("makale")->where('dergi_id',session()->get("var_olan_dergi"))->get();
       return view('dergi.DergiBilgisi',['bilgiler'=>$bilgiler,'makaleler'=>$dergide_bulunan_makaleler]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\makale_paylas  $makale_paylas
     * @return \Illuminate\Http\Response
     */
    public function show(makale_paylas $makale_paylas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\makale_paylas  $makale_paylas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dergi_bilgisi=DB::table('dergi')->where('id',$id)->get();
         session()->put("var_olan_dergi",$id);
        session()->put('dergi_bilgisi',$dergi_bilgisi);
        return redirect("paylas");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\makale_paylas  $makale_paylas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, makale_paylas $makale_paylas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\makale_paylas  $makale_paylas
     * @return \Illuminate\Http\Response
     */
    public function destroy(makale_paylas $makale_paylas)
    {
        //
    }
}
