<?php

namespace App\Http\Controllers;
use App\Makale;
use App\dergi_editor;
use Illuminate\Http\Request;
use DB;
class dergi_editor_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $makale_listele=DB::table('makale')->get();
        $dergi_listele=DB::table('dergi')->get();
        return view('layouts.index',['dergiler'=>$dergi_listele,'makaleler'=>$makale_listele]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dergi_ekle=new dergi_editor();
        $dergi_ekle->adi=$request->adi;
        $dergi_ekle->editor_adi=$request->editor_adi;
        $dergi_ekle->editor_email=$request->email;
        $dergi_ekle->editor_sifre=$request->sifre;
        $dergi_ekle->aciklama=$request->aciklama;
        $dergi_ekle->save();
        
        return redirect('dergi');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dergi_editor  $dergi_editor
     * @return \Illuminate\Http\Response
     */
    public function show(dergi_editor $dergi_editor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dergi_editor  $dergi_editor
     * @return \Illuminate\Http\Response
     */
    public function edit(dergi_editor $dergi_editor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dergi_editor  $dergi_editor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dergi_editor $dergi_editor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dergi_editor  $dergi_editor
     * @return \Illuminate\Http\Response
     */
    public function destroy(dergi_editor $dergi_editor)
    {
        //
    }
public function dergi_listele(){
    $dergi_listele=DB::table('dergi')->get();
    return view('dergi.DergiListele',['dergiler'=>$dergi_listele]);
}
}
