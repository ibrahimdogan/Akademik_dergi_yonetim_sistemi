<?php

namespace App\Http\Controllers;

use App\Makale;
use App\dergi_editor;
use DB;
use Illuminate\Http\Request;

class MakeleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
            $giris_yapmismi=session()->get("tur");
            if ($giris_yapmismi=="") {
                return redirect("/");
            } 
       
        if(session()->get("tur")=="editor"){
         $makale_listele=DB::table('makale')->where('dergi_id',session()->get('dergi_id'))->get();
        return view('makale.Makale_takip',['makaleler'=>$makale_listele]);
        }
        else{
            $makale_listele=DB::table('makale')->where('yazar_id',session()->get('yazar_id'))->get();
            return view('makale.Makale_takip',['makaleler'=>$makale_listele]);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
        $dergi_listesi=DB::table("dergi")->get();
         return view('makale.YeniMakale',['dergiler'=>$dergi_listesi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   
        $makale_ekle=new Makale();
        $makale_ekle->adi=$request->adi;
        $makale_ekle->yazar_id=session()->get('yazar_id');
        $makale_ekle->alan=$request->alan;
        $makale_ekle->url=$request->url;
        $makale_ekle->aciklama=$request->aciklama;
        $makale_ekle->dergi_id=$request->dergi_id;
        $makale_ekle->editor_onay=0;
        $makale_ekle->hakem_id=0;
        $makale_ekle->hakem_onay=0;
        $makale_ekle->mesaj="Daha Okunamadı";
        $makale_ekle->save();
        return redirect("/");
        
         
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Makale  $makale
     * @return \Illuminate\Http\Response
     */
    public function show(Makale $makale)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Makale  $makale
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
       $dergiler=DB::table('dergi')->get();
       $guncelenecek_veri=Makale::find($id);
      return view('makale.YeniMakale',['makaleler'=>$guncelenecek_veri,'dergiler'=>$dergiler]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Makale  $makale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
        $guncelle=Makale::find($id);
       if($request->onaylama_durumu==1){
        $guncelle->editor_onay=1;
        $guncelle->mesaj="editor onayladı hakemlere gönderildi";
        $guncelle->hakem_id=$request->hakem_id;
        $guncelle->save();
       }
       elseif($request->onaylama_durumu==3){
        $guncelle->mesaj=$request->mesaj;
        $guncelle->dergi_id=0;
        $guncelle->editor_onay=0;
        $guncelle->save();
       }
       else{
        $guncelle->editor_onay=0;
        $guncelle->mesaj=$request->mesaj;
        $guncelle->save();
       }
        return redirect("makale");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Makale  $makale
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
        $sil=Makale::find($id);
        $sil->delete();
        return redirect("makale");
    }

    public function makale_onayla($id)
    {
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
        $makale_onayla=DB::table('makale')->where('id',$id)->get();
        $yazar_listele=DB::table('yazar')->get();
        return view('makale.Editor_onay',['makale_onay'=>$makale_onayla,'yazarlar'=>$yazar_listele]);
    }
    public function makale_guncelle(Request $request,$id){
        $giris_yapmismi=session()->get("tur");
        if ($giris_yapmismi=="") {
            return redirect("/");
        } 
        $makale_guncelle=Makale::find($id);
        $makale_guncelle->adi=$request->adi;
        $makale_guncelle->url=$request->url;
        $makale_guncelle->aciklama=$request->aciklama;
        $makale_guncelle->save();
        return redirect("makale");
    }

}
