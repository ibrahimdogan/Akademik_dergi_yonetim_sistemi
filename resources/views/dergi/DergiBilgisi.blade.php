
@include ('layouts.menu')

<?php

foreach($bilgiler as $il){
    ?>
    <!-- end header -->
    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="span4">
            <div class="inner-heading">
              <h2><?php echo $il->adi; ?></h2>
            </div>
          </div>
          <div class="span8">
            <ul class="breadcrumb">
    
            </ul>
          </div>
        </div>
      </div>
    </section>
    <section id="content">
      <div class="container">
        <div class="row">
          <div class="span8">
            <article>
              <div class="top-wrapper">
                <div class="post-heading">
                  <h3><a href="#">Dergi ile ilgili görseller</a></h3>
                </div>
                <!-- start flexslider -->
                <div class="flexslider">
                  <ul class="slides">
                    <li>
                      <img src="img/works/full/image-01-full.jpg" alt="" />
                    </li>
                    <li>
                      <img src="img/works/full/image-02-full.jpg" alt="" />
                    </li>
                    <li>
                      <img src="img/works/full/image-03-full.jpg" alt="" />
                    </li>
                  </ul>
                </div>
                <!-- end flexslider -->
              </div>
              <p>
              <?php echo $il->aciklama; ?>
              </p>
            </article>
          </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Dergi Bilgileri</h5>
                <ul class="folio-detail">
                  <li><label>Dergi adı :</label> <?php echo $il->adi; ?></li>
                  <li><label>Editör Adı :</label> <?php echo $il->editor_adi; ?></li>
                  <li><label>Yayın Tarihi:</label> <?php echo $il->created_at; ?></li>
                  <li><label>Dergi id :</label><a href="#"><?php echo $il->id; ?></a></li>
                </ul>
              </div>
              <div class="widget">
                <h5 class="widgetheading">Dergi Açıklama</h5>
                <p>
                <?php echo $il->aciklama; ?>
                </p>
              </div>
              <div class="widget">
                <h5 class="widgetheading">Dergide bulunan Makalerler</h5>
               @foreach($makaleler as $makale)
                <p>{{$makale->adi}}</p>
                @endforeach
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>
<?php } ?>