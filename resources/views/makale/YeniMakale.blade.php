<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@if(isset($makaleler))
<form class="form-horizontal" method="get" action="/makale_guncelle/1">
@else
<form class="form-horizontal" method="post" action="/makale">
@endif
{{ csrf_field() }}
@if(isset($makaleler))
        <input type="hidden" name="_method" value="PUT">
    @endif
<fieldset>

<!-- Form Name -->
<legend>MAkale Kayıt Formu </legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="adi">Makale Adı</label>  
  <div class="col-md-4">
  <input id="adi" name="adi" type="text" value="{{isset($makaleler)? ''.$makaleler->adi:''}}" placeholder="Makale adınızı giriniz" class="form-control input-md" required="">
    
  </div>
</div>
<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="url">Makale seç</label>
  <div class="col-md-4">
    <input id="url" name="url" value="{{isset($makaleler)?''.$makaleler->url:''}}" class="input-file" type="file" required="">
  </div>
</div>
<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="alan">Konu(alan)</label>
  <div class="col-md-4">
    <select id="alan" name="alan" class="form-control">
      <option value="YAPAY ZEKA">YAPAY ZEKA</option>
      <option value="MAKİNE ÖĞRENMESİ">MAKİNE ÖĞRENMESİ</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="dergi_id">dergi seç</label>
  <div class="col-md-4">
    <select id="dergi_id" name="dergi_id" class="form-control">
      @foreach($dergiler as $dergi )
      <option value={{$dergi->id}}>{{$dergi->adi}}</option>    
      @endforeach
    </select>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="aciklama">Açıklama</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="aciklama" name="aciklama">{{isset($makaleler)? ''.$makaleler->aciklama:''}}</textarea>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="button1id"></label>
  <div class="col-md-8">
    <button id="button1id" name="button1id" class="btn btn-success">KAYDET</button>
    <button id="" name="" class="btn btn-danger">İPTAL</button>
  </div>
</div>

</fieldset>
</form>

