<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@foreach ($makale_onay as $makale)
<form class="form-horizontal" action="../yazar/{{$makale->id}}" method="post">
<input type="hidden" name="_method" value="PUT">
<fieldset>
{{csrf_field()}}
<!-- Form Name -->
<legend>Form Name</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="makale_adi">Makale_adı</label>  
  <div class="col-md-4">
  <input id="makale_adi" name="makale_adi" type="text" placeholder={{$makale->adi}} class="form-control input-md">
  </div>
</div>

<!-- Textarea -->

<div class="form-group">
  <label class="col-md-4 control-label" for="aciklama">aciklaması</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="aciklama" name="aciklama">{{$makale->aciklama}}</textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="eksik">Eksiklikler</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="eksik" name="mesaj"></textarea>
  </div>
</div>



@endforeach
<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="onay_durumu">Onay_durumu</label>
  <div class="col-md-4">
    <select id="onay_durumu" name="onaylama_durumu" class="form-control">
      <option value="1">Onaylandı</option>
      <option value="2">Düzeltilmesini iste</option>
      <option value="3">Onaylanmadı</option>
    </select>
  </div>
</div>
<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button id="" name="" class="btn btn-primary">işlemi tamamla</button>
  </div>
</div>

</fieldset>
</form>
