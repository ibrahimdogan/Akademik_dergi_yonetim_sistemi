
@include ('layouts.menu')

<?php

    ?>
    <!-- end header -->
    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="span4">
            <div class="inner-heading">
              <h2>{{$yazar_bilgi->adi}}</h2>
            </div>
          </div>
          <div class="span8">
            <ul class="breadcrumb">
            </ul>
          </div>
        </div>
      </div>
    </section>
    <section id="content">
      <div class="container">
        <div class="row">
          <div class="span8">
            <article>
              <div class="top-wrapper">
                <div class="post-heading">
                  <h3><a href="#">Sistemde Yayınlanan makaleleri</a></h3>
                </div>
                <!-- start flexslider -->
                <div class="flexslider">
                  <ul class="slides">
                      @foreach($makaleler as $makale)
                      @if($makale->hakem_onay==1)
                    <li>
                      <img src="img/works/full/{{$makale->url}}" alt="{{$makale->aciklama}}" />
                    </li>
                    
                  </ul>
                </div>
              </div>
              <p>
                 <h3> {{$makale->adi}}</h3>
              </p>
            </article>
          </div>
          @endif
        @endforeach
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Yazar Bilgileri</h5>
                <ul class="folio-detail">
                  <li><label>Yazar adı :</label>{{$yazar_bilgi->adi}}</li>
                  <li><label>alanı :</label> {{$yazar_bilgi->alan}}</li>
                  <li><label>Kayıt Tarihi:</label> {{$yazar_bilgi->created_at}}</li>
                </ul>
              </div>
              <div class="widget">
                <h5 class="widgetheading">Yazar Hakkında</h5>
                <p>
                {{$yazar_bilgi->biyografi}}
                </p>
              </div>
             
            </aside>
          </div>
        </div>
      </div>
    </section>
@include ('layouts.footer')