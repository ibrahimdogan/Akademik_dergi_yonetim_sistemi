@include ('layouts.menu')
  
      <div class="container">
        <div class="row">
          <div class="span12">
            <h4> <strong>Yazar Kayıt</strong></h4>

            <form action="/yazar" method="post" role="form" class="contactForm">
            {{ csrf_field() }}

              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>

              <div class="row">
                <div class="span4 form-group">
                  <input type="text" name="adi" class="form-control" id="name" placeholder="Adınızı giriniz" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
              
                <div class="span4 form-group">
                  <input type="text" name="soyadi" class="form-control" id="name" placeholder="Soyadınızı giriniz" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                
                <div class="span4 form-group">
                  <input type="email" name="email" class="form-control" id="name" placeholder="Email giriniz" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                <br> <br> <br>
                <div class="span4 form-group">
                          <!-- Select Basic -->
                <div class="col-md-8">
                    <select id="alan" name="alan" class="form-control">
                    <option value="YAPAY ZEKA">YAPAY ZEKA</option>
                    <option value="MAKİNE ÖĞRENMESİ">MAKİNE ÖĞRENMESİ</option>
                    </select>
                </div>
                    </div>
                    <div class="span4 form-group">
                  <input type="text" name="sifre" class="form-control" id="name" placeholder="Şifre giriniz" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>

                  <div class="span4 form-group">
                  <input type="text" name="tekrar" class="form-control" id="name" placeholder="Şifre (tekrar)" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                
                <div class="span12 margintop10 form-group">
                <h3>Biyografi</h3>

                  <textarea class="form-control" name="biyografi" rows="12" data-rule="required" data-msg="Please write something for us" placeholder="biyaografi"></textarea>
                  <div class="validation"></div>
                  <p class="text-center">
                    <button class="btn btn-large btn-theme margintop10" type="submit">Yazar Kaydet</button>
                  </p>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    @include ('layouts.footer')
