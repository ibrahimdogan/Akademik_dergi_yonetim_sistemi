
@include('layouts.menu')
    </section>
    <section id="content">
      <div class="container">
        <div class="row">
          <div class="span4">
            <aside class="left-sidebar">
              <div class="widget">
                <form class="form-search">
                  <input placeholder="Type something" type="text" class="input-medium search-query">
                  <button type="submit" class="btn btn-square btn-theme">Search</button>
                </form>
              </div>
              <div class="widget">
                <h5 class="widgetheading">Kategoriler</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="#">Bilişim</a><span> (20)</span></li>
                  <li><i class="icon-angle-right"></i><a href="#">Sağlık</a><span> (11)</span></li>
                  <li><i class="icon-angle-right"></i><a href="#">Enerji</a><span> (9)</span></li>
                  <li><i class="icon-angle-right"></i><a href="#">Spor</a><span> (12)</span></li>
                  <li><i class="icon-angle-right"></i><a href="#">Tarım</a><span> (18)</span></li>
                </ul>
              </div>
              
              <div class="widget">
                <h5 class="widgetheading">Popular tags</h5>
                <ul class="tags">
                  <li><a href="#">Web design</a></li>
                  <li><a href="#">Trends</a></li>
                  <li><a href="#">Technology</a></li>
                  <li><a href="#">Internet</a></li>
                  <li><a href="#">Tutorial</a></li>
                  <li><a href="#">Development</a></li>
                </ul>
              </div>
            </aside>
          </div>
          <div class="span8">
            <article>
              <div class="row">
                <div class="span8">
                  <div class="post-image">
                    <div class="post-heading">
                      <h3><a href="#">Hakkımızda Kısa Bilgi</a></h3>
                    </div>
                  </div>
                  <p>
                  Müşterilerinin yanı sıra çalışanları, iş ortakları ve hatta rakipleri için bir tutkuya dönüşecek, sınıfının en iyisi yaşam tarzı markalarını bünyesinde barındıran Doğuş, çalıştığı alanlarda küresel bir oyuncu olma hedefiyle çalışmalarına devam ediyor.

Otomotiv, inşaat, medya, turizm ve hizmetler, gayrimenkul, enerji ve yeme-içme olmak üzere yedi sektörde faaliyet gösteren Doğuş Grubu ayrıca, mevcut hizmet verdiği sektörlerin yanı sıra teknoloji, spor ve eğlence alanındaki yeni yatırımlarıyla da büyümesini sürdürüyor. Grup 300’ün üzerindeki şirketi ve 35 bini aşkın çalışanıyla müşterilerine üstün teknoloji, yüksek marka kalitesi ve dinamik bir insan kaynağı ile hizmet veriyor.
         </p>
                  <blockquote>
                    <i class="icon-quote-left"></i> Lorem ipsum dolor sit amet, ei quod constituto qui. Summo labores expetendis ad quo, lorem luptatum et vis. No qui vidisse signiferumque...
                  </blockquote>
                  <p>
                  
Doğuş Grubu’nun ulaştığı başarının arkasında, müşteri odaklı ve verimliliği merkez alan bir yönetim yaklaşımı yer alıyor. Bu yaklaşımın bir sonucu olarak, Grup dünya ölçeğinde saygın markalarla yaptığı iş birlikleriyle, Türkiye’yi bütün dünyada temsil ediyor. Doğuş Grubu’nun, otomotivde Volkswagen AG ve TÜVSÜD, medyada Condé Nast (Vogue, GQ, Traveller), turizmde Hyatt International Ltd, marinacılıkta Latsis Grubu, Kiriacoulis Grubu ve Adriatic Croatia International (ACI) Grubu’nun yanı sıra yeme-içme ve eğlence sektöründe de bünyesinde Coya, Roka, Zuma ve Oblix gibi markaları bulunduran uluslararası Azumi Grubu ve e-ticaret alanında Güney Koreli SK Group gibi büyük küresel oyuncularla iş birlikleri ve ortaklıkları bulunuyor.
         
                  </p>
                  <div class="bottom-article">
                    <ul class="meta-post">
                      <li><i class="icon-user"></i><a href="#"> Admin</a></li>
                      <li><i class="icon-folder-open"></i><a href="#"> Blog</a></li>
                      <li><i class="icon-tags"></i><a href="#">Web design</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </article>
            <!-- author info -->
            <div class="about-author">
              <a href="#" class="thumbnail align-left"><img src="img/avatar.png" alt="" /></a>
              <h5><strong><a href="#">John doe</a></strong></h5>
              <p>
              Doğuş Grubu’nun yönetim yaklaşımı, bütün toplumun yararlandığı ve yararlanacağı kurumsal yurttaşlık bilincini de yansıtıyor. Doğuş Grubu, sosyal sorumluluk çalışmalarını yürütürken insanların hayatlarında bir iz bırakmayı ve günlük yaşamı daha iyi bir hale getirmeyi amaçlıyor. Grubun sosyal sorumluluk projeleri toplumun sürekli ileriye giden ve gelişen bir gelecek yaratmasına katkıda bulunmak hedefiyle yönetiliyor.

            </div>
            <div class="comment-area">
              <h4>4 Comments</h4>
              <div class="media">
                <a href="#" class="thumbnail pull-left"><img src="img/avatar.png" alt="" /></a>
                <div class="media-body">
                  <div class="media-content">
                    <h6><span>March 12, 2013</span> Karen medisson</h6>
                    <p>
                    Grup, özellikle kültür-sanat, eğitim ve spor alanlarına odaklanan çeşitli kurumsal sosyal sorumluluk ve sponsorluk projelerini hayata geçiriyor. Destek verdiği tüm alanlarda, sorumluluklarının bilincinde olan Doğuş Grubu, topluma öncü ve örnek olma amacıyla hareket ederken, yatırımlarıyla ülke ekonomisine ve istihdama katkı sağlıyor.              </p>
                    </p>
                    <a href="#" class="align-right">Reply comment</a>
                  </div>
                </div>
              </div>
              <div class="media">
                <a href="#" class="thumbnail pull-left"><img src="img/avatar.png" alt="" /></a>
                <div class="media-body">
                  <div class="media-content">
                    <h6><span>March 12, 2013</span> Smith sanderson</h6>
                    <p>
                      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                    </p>
                    <a href="#" class="align-right">Reply comment</a>
                  </div>
                  <div class="media">
                    <a href="#" class="thumbnail pull-left"><img src="img/avatar.png" alt="" /></a>
                    <div class="media-body">
                      <div class="media-content">
                        <h6><span>March 12, 2013</span> Thomas guttenberg</h6>
                        <p>
                          Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                        </p>
                        <a href="#" class="align-right">Reply comment</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="media">
                <a href="#" class="thumbnail pull-left"><img src="img/avatar.png" alt="" /></a>
                <div class="media-body">
                  <div class="media-content">
                    <h6><span>March 12, 2013</span> Vicky lumora</h6>
                    <p>
                      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                    </p>
                    <a href="#" class="align-right">Reply comment</a>
                  </div>
                </div>
              </div>
              <h4>Leave your comment</h4>
              <form id="commentform" action="#" method="post" name="comment-form">
                <div class="row">
                  <div class="span4">
                    <input type="text" placeholder="* Enter your full name" />
                  </div>
                  <div class="span4">
                    <input type="text" placeholder="* Enter your email address" />
                  </div>
                  <div class="span4 margintop10">
                    <input type="text" placeholder="Enter your website" />
                  </div>
                  <div class="span8 margintop10">
                    <p>
                      <textarea rows="12" class="input-block-level" placeholder="*Your comment here"></textarea>
                    </p>
                    <p>
                      <button class="btn btn-theme margintop10" type="submit">Submit comment</button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

