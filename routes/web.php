<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//genel işlemler
Route::get('/hakkimizda', function () {
    return view('layouts.hakkimizda');
});

//genel işlemler
Route::get('/iletisim', function () {
    return view('layouts.iletisim');
});



Route::get('/yazar_ekle', function () {
    return view('yazar.YeniYazarEkle');
});
//yazar giriş çıkış işlemleri
Route::get('/cikis','yazar_hakem_controller@cikis');
Route::post('/giris_kontrol','yazar_hakem_controller@giris_kontrol');
Route::resource('yazar','yazar_hakem_controller');

//editor dergi işlemleri
Route::get('/dergi_ekle', function () {
    return view('dergi.YeniDergiEkle');
});
Route::get('/','dergi_editor_controller@index');
Route::resource('dergi','dergi_editor_controller');
Route::get('/dergi_listele','dergi_editor_controller@dergi_listele');

//makale paylaşma işlemleri
Route::resource('paylas','makale_paylas_controller');
Route::get('makale_guncelle/{id}','MakeleController@makale_guncelle');

//makale sürec işlemleri
Route::resource('/makale','MakeleController');
Route::get('/makale_onayla_devam/{id}','MakeleController@makale_onayla');
Route::get('/yazar_onayla_devam/{id}','yazar_hakem_Controller@yazar_onayla');
Route::get('/yazar_profil','yazar_hakem_Controller@yazar_profil');

//hata oluştugunda yapılacaklar
Route::get('/bulunamadı', function () {
    return view('layouts.hata');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
